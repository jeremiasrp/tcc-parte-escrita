\babel@toc {brazilian}{}
\setcounter {tocdepth}{5}
\contentsline {chapter}{\numberline {1}INTRODU\IeC {\c C}\IeC {\~A}O}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}\hspace {-0.3cm}Objetivos}{6}{section.1.1}
\contentsline {section}{\numberline {1.2}\hspace {-0.3cm}Objetivos Espec\IeC {\'\i }ficos}{6}{section.1.2}
\contentsline {chapter}{\numberline {2}REFERENCIAL TE\IeC {\'O}RICO}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}\hspace {-0.3cm}Tecnologias assistivas para locomo\IeC {\c c}\IeC {\~a}o}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}\hspace {-0.3cm}Solu\IeC {\c c}\IeC {\~o}es dispon\IeC {\'\i }veis para travessias de ruas}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}\hspace {-0.3cm}Arquitetura da BHTRANS}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}\hspace {-0.3cm}Funcionamento da central semaf\IeC {\'o}rica}{9}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}METODOLOGIA}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}\hspace {-0.3cm}Requisitos do Aplicativo}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}\hspace {-0.3cm}Arquitetura}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}\hspace {-0.3cm}IONIC \textit {Framework}}{13}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}\hspace {-0.3cm}\textit {Plugins} Cordova}{14}{subsubsection.3.2.1.1}
\contentsline {subsection}{\numberline {3.2.2}\hspace {-0.3cm}WEB API}{14}{subsection.3.2.2}
\contentsline {chapter}{\numberline {4}DESENVOLVIMENTO E RESULTADOS}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}\hspace {-0.3cm}Estrutura de arquivos do projeto em \textit {IONIC}}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}\hspace {-0.3cm}Google \textit {Maps API}}{16}{section.4.2}
\contentsline {section}{\numberline {4.3}\hspace {-0.3cm}GPS}{17}{section.4.3}
\contentsline {section}{\numberline {4.4}\hspace {-0.3cm}Servi\IeC {\c c}os}{17}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}\hspace {-0.3cm}Localiza\IeC {\c c}\IeC {\~a}o}{17}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}\hspace {-0.3cm}Banco de Dados SQlite}{18}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}\hspace {-0.3cm}Sem\IeC {\'a}foros}{18}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}\hspace {-0.3cm}Sentido}{19}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}\hspace {-0.3cm}Fun\IeC {\c c}\IeC {\~a}o \textit {start}}{20}{section.4.5}
\contentsline {section}{\numberline {4.6}\hspace {-0.3cm}Testes}{22}{section.4.6}
\babel@toc {brazilian}{}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{25}{chapter*.1}
